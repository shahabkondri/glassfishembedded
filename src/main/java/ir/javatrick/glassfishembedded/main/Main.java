package ir.javatrick.glassfishembedded.main;

import org.glassfish.embeddable.*;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by shahab on 7/11/14.
 */
public class Main {
    public static void main(String[] args) {
        try {
            String domainPath = Main.class.getResource("/META-INF/domain.xml").toString();
            URI deployerPath = Main.class.getResource("/META-INF/helloworld.war").toURI();

            GlassFishProperties properties = new GlassFishProperties();
            properties.setConfigFileURI(domainPath);
            properties.setConfigFileReadOnly(true);

            GlassFishRuntime runtime = GlassFishRuntime.bootstrap();
            GlassFish glassFish = runtime.newGlassFish(properties);
            glassFish.start();

            Deployer deployer = glassFish.getDeployer();
            deployer.deploy(deployerPath);

        } catch (GlassFishException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
